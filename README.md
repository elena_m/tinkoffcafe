# TinkoffCafe
This project aims to create a model that could predict meal choices
of Tinkoff Bank staff members baised on known food preferences.
Our goal is to analyze data consisting of buffet orders (what each
bank worker ordered and when), build a prediction model for what
they are going to order next time, asses its efficiency using
Mean F1-Score and design a WEB interface that would allow
for convenient access to the prediction algorithm.

The project is being developed by Saint Petersburg State
University students commissioned by Tinkoff Bank.

