from django.shortcuts import render, redirect
from .forms import FileUploadForm

def file_upload(request):
	if request.method == 'POST':
		form = FileUploadForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return render(request, 'data_analysis/success.html')
	else:
		form = FileUploadForm()
	return render(request, 'data_analysis/index.html', {'form': form})
